﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Harry_Potter
{
    public class HarryPotter
    {
        private readonly int _bookPrice = 100;

        private readonly Dictionary<int, double> _discountLookUp = new Dictionary<int, double>()
        {
            {1, 1.0},
            {2, 0.95},
            {3, 0.9},
            {4, 0.8},
            {5, 0.75},
        };

        public Double CheckOut(Dictionary<string, int> shoppingCar)
        {

            var totalFee = 0;

            for (int i = shoppingCar.Values.Max();  i > 0 ; i--)
            {
                var maxCountBookSet = shoppingCar.Where(x => x.Value >= i);
                totalFee += (int)(_bookPrice * maxCountBookSet.Count() * _discountLookUp[maxCountBookSet.Count()]);
            }

            
            return totalFee;
        }
    }
}